package middlewares

import (
	"finale-project/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
)

func JwtAuthMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}
		c.Next()
	}
}

func SellerMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}
		admin, _ := token.ExtractAdmin(c)
		if admin {
			c.Next()
		}
		seller, _ := token.ExtractSeller(c)
		if !seller {
			c.Abort()
			return
		}
		c.Next()
	}
}

func AdminMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := token.TokenValid(c)
		if err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}
		admin, _ := token.ExtractAdmin(c)

		if !admin {
			c.Abort()
			return
		}
		c.Next()
	}
}
