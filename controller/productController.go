package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type productControllerInput struct {
	Name        string `json:"name"`
	Desc        string `json:"desc"`
	Sku         string `json:"sku"`
	ImageUrl    string `json:"image_url"`
	Price       int    `json:"price"`
	Stock       int    `json:"stock"`
	Category_id uint   `json:"category_id"`
}

// GetAllProduct godoc
// @Summary Get all Product.
// @Description Get a list of Product.
// @Tags Product
// @Produce json
// @Success 200 {object} []models.Product
// @Router /products [get]
func GetAllProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var products []models.Product

	db.Find(&products)
	c.JSON(http.StatusOK, gin.H{"data": products})
}

// CreateProduct godoc
// @Summary Create New Product.
// @Description Creating a new Product.
// @Tags Product
// @Param Body body productControllerInput true "the body to create a new Product"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Product
// @Router /products [post]
func CreateProduct(c *gin.Context) {
	// validate input

	var input productControllerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}
	// create product
	userid, _ := token.ExtractTokenId(c)
	product := models.Product{
		Name:       input.Name,
		Desc:       input.Desc,
		Sku:        input.Sku,
		ImageUrl:   input.ImageUrl,
		Price:      input.Price,
		Stock:      input.Stock,
		CategoryID: input.Category_id,
		UserID:     userid,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&product)

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetProductById godoc
// @Summary Get Product.
// @Description Get a Product by id.
// @Tags Product
// @Produce json
// @Param id path string true "Product id"
// @Success 200 {object} models.Product
// @Router /products/{id} [get]
func GetProductByid(c *gin.Context) {
	var product models.Product
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id=?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": product})
}

// UpdateProduct godoc
// @Summary Update Product.
// @Description Update Product by id and user id.
// @Tags Product
// @Produce json
// @Param id path string true "Product id"
// @Param Body body productControllerInput true "the body to update category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Product
// @Router /products/{id} [patch]
func UpdateProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	userid, _ := token.ExtractTokenId(c)
	var product models.Product
	if err := db.Where("id=? and user_id=?", c.Param("id"), userid).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}
	// validate input
	var input productControllerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	updateinput := models.Product{
		Name:       input.Name,
		Desc:       input.Desc,
		Sku:        input.Sku,
		ImageUrl:   input.ImageUrl,
		Price:      input.Price,
		Stock:      input.Stock,
		CategoryID: input.Category_id,
		UserID:     userid,
	}

	db.Model(&product).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"data": product})
}

// DeleteProduct godoc
// @Summary Delete one Product.
// @Description Delete a Product by id and user id.
// @Tags Product
// @Produce json
// @Param id path string true "Product id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /products/{id} [delete]
func DeleteProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	userid, _ := token.ExtractTokenId(c)
	var product models.Product
	if err := db.Where("id=? and user_id=?", c.Param("id"), userid).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
	}
	db.Delete(&product)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
