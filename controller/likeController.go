package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Like godoc
// @Summary Post like by user.
// @Description User give like to product.
// @Tags Like
// @Param product_id path string true "Like product_id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /like/{product_id} [post]
func PostLike(c *gin.Context) {
	userid, _ := token.ExtractTokenId(c)
	param := c.Param("product_id")
	productid, err := strconv.Atoi(param)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	like := models.Like{
		Like:      true,
		ProductID: uint(productid),
		UserID:    userid,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&like)
	c.JSON(http.StatusOK, gin.H{"data": true})
}

// Like godoc
// @Summary Get all likes.
// @Description Get all likes by Product id.
// @Tags Like
// @Param product_id path string true "Like product_id"
// @Produce json
// @Success 200 {object} map[string]int64
// @Router /like/{product_id} [get]
func GetLikebyProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var likes models.Like

	if err := db.Where("product_id=?", c.Param("product_id")).Find(&likes).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "record not found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": likes})
}

// Like godoc
// @Summary Delete Like.
// @Description Deleting like by product id and user id.
// @Tags Like
// @Param product_id path string true "Like product_id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /like/{product_id} [delete]
func Deletelike(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var like models.Like
	userid, _ := token.ExtractTokenId(c)
	if err := db.Where("product_id=? and user_id=?", c.Param("id"), userid).First(&like).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "record not found"})
		return
	}
	db.Delete(&like)
	c.JSON(http.StatusOK, gin.H{"data": true})

}
