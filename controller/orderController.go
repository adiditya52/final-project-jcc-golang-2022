package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type orderinput struct {
	Quantity int `json:"quantity"`
}

// Order godoc
// @Summary Create New Order.
// @Description Creating a new Order.
// @Tags Order
// @Param product_id path string true "Order product_id"
// @Param Body body orderinput true "the body to create a new order"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Order
// @Router /order/{product_id} [post]
func CreateOrder(c *gin.Context) {
	var quantity orderinput
	if err := c.ShouldBindJSON(&quantity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	iduser, _ := token.ExtractTokenId(c)

	db := c.MustGet("db").(*gorm.DB)
	id := c.Param("productID")
	productID, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	order := models.Order{
		ProductID: uint(productID),
		UserID:    iduser,
		Quantity:  quantity.Quantity,
	}

	db.Create(&order)
	c.JSON(http.StatusOK, gin.H{"data": order})

}

// Order godoc
// @Summary Update an Order.
// @Description Updating an Order.
// @Tags Order
// @Param product_id path string true "Order product_id"
// @Param Body body orderinput true "the body to update an order"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Order
// @Router /order/{product_id} [patch]

func UpdateOrdeer(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var quantity orderinput
	if err := c.ShouldBindJSON(&quantity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	iduser, _ := token.ExtractTokenId(c)
	id := c.Param("productID")
	productID, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var order models.Order
	if err := db.Where("user_id=? and product_id=?", iduser, productID).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}

	updateinput := models.Order{
		Quantity: quantity.Quantity,
	}

	db.Model(order).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"data": order})

}

// Order godoc
// @Summary Get all Order.
// @Description Get all Order by User id.
// @Tags Order
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.Order
// @Router /order [get]
func GetOrder(c *gin.Context) {
	iduser, _ := token.ExtractTokenId(c)
	var orders []models.Order

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("user_id=?", iduser).Find(&orders); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": orders})
}

// Order godoc
// @Summary Delete an Order.
// @Description Deleting an Order filter by product id and user id.
// @Tags Order
// @Param product_id path string true "Order product_id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /order/{product_id} [delete]
func DeleteOrder(c *gin.Context) {
	iduser, _ := token.ExtractTokenId(c)
	id := c.Param("productID")
	productID, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db := c.MustGet("db").(*gorm.DB)
	var order models.Order
	if err := db.Where("user_id=? and product_id=?", iduser, productID).First(&order).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}
	db.Delete(&order)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
