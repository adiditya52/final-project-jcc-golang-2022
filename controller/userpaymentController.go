package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type userpaymentinput struct {
	PaymentType string `json:"payment_type"`
	Provider    string `json:"provider"`
	AccountNo   string `json:"account_no"`
}

// UserPayment godoc
// @Summary Create New UserPayment.
// @Description Creating a new UserPayment.
// @Tags UserPayment
// @Param Body body userpaymentinput true "the body to create a new UserPayment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.UserPayment
// @Router /user-payment [post]
func CreateUserpayment(c *gin.Context) {
	// validate input
	var input userpaymentinput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}
	// create product
	iduser, _ := token.ExtractTokenId(c)

	userpayment := models.UserPayment{
		PaymentType: input.PaymentType,
		Provider:    input.Provider,
		AccountNo:   input.AccountNo,
		UserID:      iduser,
		CreateAt:    time.Now(),
		UpdateAt:    time.Now(),
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&userpayment)

	c.JSON(http.StatusOK, gin.H{"data": userpayment})
}

// UserPayment godoc
// @Summary Get UserPayment by UserID.
// @Description Get UserPayment.
// @Tags UserPayment
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.UserPayment
// @Router /user-payment [get]
func GetUserpayment(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var userpayment models.UserPayment

	iduser, _ := token.ExtractTokenId(c)
	if err := db.Where("user_id=?", iduser).First(&userpayment).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": userpayment})
}

// UserPayment godoc
// @Summary Update Userpayment by UserID.
// @Description Update a UserPayment.
// @Tags UserPayment
// @Param Body body userpaymentinput true "the body to create a new userpayment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.UserPayment
// @Router /user-payment [patch]
func UpdateUserpayment(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var userpayment models.UserPayment

	iduser, _ := token.ExtractTokenId(c)
	db.Where("user_id=?", iduser).First(&userpayment)

	var input userpaymentinput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	updateinput := models.UserPayment{
		PaymentType: input.PaymentType,
		Provider:    input.Provider,
		AccountNo:   input.AccountNo,
		UpdateAt:    time.Now(),
	}
	db.Model(&userpayment).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"data": userpayment})
}

// UserPayment godoc
// @Summary Delete UserPayment.
// @Description Delete a UserPayment.
// @Tags UserPayment
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /user-payment [delete]
func DeleteUserpayment(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var userpayment models.UserPayment
	iduser, _ := token.ExtractTokenId(c)
	db.Where("user_id=?", iduser).First(&userpayment)

	db.Delete(&userpayment)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
