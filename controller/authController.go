package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type loginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}
type registerInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
}

type tokenInput struct {
	Token int `json:"token"`
}

// LoginUser godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body loginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input loginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	u := models.User{
		Username: input.Username,
		Password: input.Password,
	}
	token, err := models.LoginCheck(u.Username, u.Password, db)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "succes", "user": u.Username, "token": token})
}

// Register godoc
// @Summary Register a user.
// @Description registering a user from public access.
// @Tags Auth
// @Param Body body registerInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input registerInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	u := models.User{
		Username: input.Username,
		Password: input.Password,
		Email:    input.Email,
		CreateAt: time.Now(),
		UpdateAt: time.Now(),
	}
	_, err := u.SaveUser(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user := map[string]string{
		"username": input.Username,
		"email":    input.Email,
	}
	c.JSON(http.StatusOK, gin.H{"message": "registration succes", "user": user})
}

// Register godoc
// @Summary Update a user.
// @Description updateing a user information.
// @Tags Auth
// @Param Body body registerInput true "the body to register a user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.User
// @Router /update [patch]
func UpdateUser(c *gin.Context) {
	iduser, _ := token.ExtractTokenId(c)
	db := c.MustGet("db").(*gorm.DB)
	var user models.User

	if err := db.Where("user_id=?", iduser).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}

	var input registerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	updateinput := models.User{
		Username: input.Username,
		Email:    input.Email,
		Password: input.Password,
		UpdateAt: time.Now(),
	}
	db.Model(&user).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"data": user})
}

// Register godoc
// @Summary Upgrade from user to seller.
// @Description Upgrading Role from user to seller.
// @Tags Auth
// @Param Body body tokenInput true "the body to register a user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]string
// @Router /upgrade-seller [post]
func UpgradeSeller(c *gin.Context) {
	iduser, _ := token.ExtractTokenId(c)
	db := c.MustGet("db").(*gorm.DB)
	var user models.User

	if err := db.Where("id=?", iduser).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}

	var input tokenInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var updateinput models.User
	if input.Token == 123456 {
		updateinput.IsSeller = true
		updateinput.UpdateAt = time.Now()
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": "wrong token"})
		return
	}

	db.Model(&user).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"message": "silahkang login ulang"})
}

// Register godoc
// @Summary Upgrade from user to admin.
// @Description Upgrading Role from user to admin.
// @Tags Auth
// @Param Body body tokenInput true "the body to register a user"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]string
// @Router /upgrade-admin [post]
func UpgradeAdmin(c *gin.Context) {
	iduser, _ := token.ExtractTokenId(c)
	db := c.MustGet("db").(*gorm.DB)
	var user models.User

	if err := db.Where("id=?", iduser).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}

	var input tokenInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var updateinput models.User
	if input.Token == 123456 {
		updateinput.IsAdmin = true
		updateinput.UpdateAt = time.Now()
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": "wrong token"})
		return
	}

	db.Model(&user).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"message": "silahkan login ulang"})
}

// Register godoc
// @Summary Delete user.
// @Description deleting user by admin.
// @Tags Auth
// @Param id path string true "User id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /delete/{id} [delete]
func DeleteUser(c *gin.Context) {
	idsu, _ := token.ExtractAdmin(c)
	if !idsu {
		c.JSON(http.StatusNonAuthoritativeInfo, gin.H{"data": false})
	}
	db := c.MustGet("db").(*gorm.DB)
	var user models.User
	if err := db.Where("id=?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}
	db.Delete(&user)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
