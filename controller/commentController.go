package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type commentInput struct {
	Comment string `json:"comment"`
}

// Comment godoc
// @Summary Create comment on a product.
// @Description Creating a new comment on product.
// @Tags Comment
// @Param product_id path string true "Comment product_id"
// @Param Body body commentInput true "the body to create a new Comment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Comment
// @Router /comment/{product_id} [post]
func PostComment(c *gin.Context) {
	userid, _ := token.ExtractTokenId(c)
	param := c.Param("product_id")
	productid, err := strconv.Atoi(param)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	var input commentInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	comment := models.Comment{
		Comment:   input.Comment,
		ProductID: uint(productid),
		UserID:    userid,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&comment)
	c.JSON(http.StatusOK, gin.H{"data": comment})
}

// Comment godoc
// @Summary Get all Comments.
// @Description Get all Comments by Product id.
// @Tags Comment
// @Param product_id path string true "Comment product_id"
// @Produce json
// @Success 200 {object} []models.Comment
// @Router /comment/{product_id} [get]
func GetCommentbyProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var comments []models.Comment

	if err := db.Where("product_id=?", c.Param("product_id")).Find(&comments).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "record not found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": comments})
}

// Comment godoc
// @Summary Delete a Comment.
// @Description Deleting a Comment by Product id.
// @Tags Comment
// @Param product_id path string true "Comment product_id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /comment/{product_id} [delete]
func DeleteComment(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var comment models.Comment
	userid, _ := token.ExtractTokenId(c)
	if err := db.Where("product_id=? and user_id=?", c.Param("id"), userid).First(&comment).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "record not found"})
		return
	}
	db.Delete(&comment)
	c.JSON(http.StatusOK, gin.H{"data": true})

}
