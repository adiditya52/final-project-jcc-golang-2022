package controller

import (
	"finale-project/models"
	"finale-project/utils/token"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type profileinput struct {
	Name        string `json:"name"`
	Address     string `json:"address"`
	City        string `json:"city"`
	PostalCode  string `json:"postal_code"`
	MobilePhone string `json:"mobile_phone"`
}

// Profile godoc
// @Summary Create New Profile.
// @Description Creating a new Profile.
// @Tags Profile
// @Param Body body profileinput true "the body to create a new Profile"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Profile
// @Router /profile [post]
func CreateProfile(c *gin.Context) {
	// validate input
	var input profileinput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}
	// create product
	iduser, _ := token.ExtractTokenId(c)

	profile := models.Profile{
		Name:        input.Name,
		Address:     input.Address,
		City:        input.City,
		PostalCode:  input.PostalCode,
		MobilePhone: input.MobilePhone,
		UserID:      iduser,
		CreateAt:    time.Now(),
		UpdateAt:    time.Now(),
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&profile)

	c.JSON(http.StatusOK, gin.H{"data": profile})
}

// Profile godoc
// @Summary Get Profile by UserID.
// @Description Get Profile.
// @Tags Profile
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Profile
// @Router /profile [get]
func GetProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var profile models.Profile

	iduser, _ := token.ExtractTokenId(c)
	if err := db.Where("user_id=?", iduser).First(&profile).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record Not Found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": profile})
}

// Profile godoc
// @Summary Update Profile by UserID.
// @Description Update a Profile.
// @Tags Profile
// @Param Body body profileinput true "the body to create a new Profile"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Profile
// @Router /profile [patch]
func UpdateProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var profile models.Profile

	iduser, _ := token.ExtractTokenId(c)
	db.Where("user_id=?", iduser).First(&profile)

	var input profileinput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	updateinput := models.Profile{
		Name:        input.Name,
		Address:     input.Address,
		City:        input.City,
		PostalCode:  input.PostalCode,
		MobilePhone: input.MobilePhone,
		UpdateAt:    time.Now(),
	}
	db.Model(&profile).Updates(updateinput)
	c.JSON(http.StatusOK, gin.H{"data": profile})
}

// Profile godoc
// @Summary Delete Profile.
// @Description Delete a Profile.
// @Tags Profile
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} map[string]boolean
// @Router /profile [delete]
func DeleteProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var profile models.Profile
	iduser, _ := token.ExtractTokenId(c)
	db.Where("user_id=?", iduser).First(&profile)

	db.Delete(&profile)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
