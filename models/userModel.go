package models

import (
	"finale-project/utils/token"
	"html"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	ID          uint   `gorm:"primary key" json:"id"`
	Username    string `gorm:"not null;unique" json:"username"`
	Email       string `gorm:"not null;unique" json:"email"`
	Password    string `gorm:"not null;unique" json:"password"`
	IsAdmin     bool   `gorm:"default:false"`
	IsSeller    bool   `gorm:"default:false"`
	CreateAt    time.Time
	UpdateAt    time.Time
	Profile     Profile     `josn:"-"`
	UserPayment UserPayment `josn:"-"`
	Orders      []Order     `json:"-"`
	Products    []Product   `json:"-"`
	Likes       []Like      `json:"-"`
	Comments    []Comment   `json:"-"`
}

func VerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func LoginCheck(username string, password string, db *gorm.DB) (string, error) {
	var err error
	u := User{}

	err = db.Model(User{}).Where("username=?", username).Take(&u).Error
	if err != nil {
		return "", err
	}
	err = VerifyPassword(password, u.Password)

	if err != nil || err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}
	token, err := token.GenerateToken(u.ID, u.IsAdmin, u.IsSeller)

	if err != nil {
		return "", err
	}
	return token, nil
}
func (u *User) SaveUser(db *gorm.DB) (*User, error) {
	// turn password to hash
	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if errPassword != nil {
		return &User{}, errPassword
	}
	u.Password = string(hashedPassword)
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))

	var err error = db.Create(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}
