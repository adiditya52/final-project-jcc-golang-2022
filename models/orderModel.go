package models

type Order struct {
	ID        uint `gorm:"primary key" json:"id"`
	Quantity  int  `json:"quantity"`
	UserID    uint `json:"user_id"`
	ProductID uint `josn:"product_id"`
}
