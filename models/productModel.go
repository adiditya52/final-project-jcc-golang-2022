package models

import "time"

type Product struct {
	ID         uint      `gorm:"primary key" json:"id"`
	Name       string    `json:"name"`
	Desc       string    `json:"desc"`
	Sku        string    `json:"sku"`
	ImageUrl   string    `json:"image_url"`
	Price      int       `json:"price"`
	Stock      int       `json:"stock"`
	CreateAt   time.Time `json:"create_at"`
	UpdateAt   time.Time `json:"update_at"`
	CategoryID uint      `json:"category_id"`
	UserID     uint      `json:"user_id"`
	Category   Category  `json:"-"`
	Orders     []Order   `json:"-"`
	Likes      []Like    `json:"-"`
	Comments   []Comment `json:"-"`
}
