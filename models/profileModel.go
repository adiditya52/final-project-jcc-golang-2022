package models

import "time"

type Profile struct {
	ID          uint      `gorm:"primary key"`
	Name        string    `json:"name"`
	Address     string    `json:"address"`
	City        string    `json:"city"`
	PostalCode  string    `json:"postal_code"`
	MobilePhone string    `json:"mobile_phone"`
	UserID      uint      `josn:"user_id"`
	CreateAt    time.Time `json:"create_at"`
	UpdateAt    time.Time `json:"update_at"`
}

type UserPayment struct {
	ID          uint      `gorm:"primary key"`
	PaymentType string    `json:"payment_type"`
	Provider    string    `json:"provider"`
	AccountNo   string    `json:"account_no"`
	UserID      uint      `json:"user_id"`
	CreateAt    time.Time `json:"create_at"`
	UpdateAt    time.Time `json:"update_at"`
}
