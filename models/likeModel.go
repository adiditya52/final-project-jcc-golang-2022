package models

type Like struct {
	ID        uint `gorm:"primary key" json:"id"`
	Like      bool `json:"like"`
	UserID    uint `json:"user_id"`
	ProductID uint `josn:"product_id"`
}

type Comment struct {
	ID        uint   `gorm:"primary key" json:"id"`
	Comment   string `json:"comment"`
	UserID    uint   `json:"user_id"`
	ProductID uint   `josn:"product_id"`
}
