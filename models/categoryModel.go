package models

import "time"

type Category struct {
	ID       uint      `gorm:"primary key" json:"id"`
	Name     string    `json:"name"`
	Desc     string    `json:"desc"`
	CreateAt time.Time `json:"create_at"`
	UpdateAt time.Time `json:"update_at"`
	Products []Product `json:"-"`
}
