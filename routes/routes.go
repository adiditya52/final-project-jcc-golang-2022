package routes

import (
	"finale-project/controller"
	"finale-project/middlewares"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"gorm.io/gorm"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.POST("/login", controller.Login)
	r.POST("/register", controller.Register)

	r.GET("/products", controller.GetAllProduct)
	r.GET("/products/:id", controller.GetProductByid)

	userUp := r.Group("")
	userUp.Use(middlewares.JwtAuthMiddleWare())
	userUp.PATCH("/update", controller.UpdateUser)
	userUp.POST("/upgrade-seller", controller.UpgradeSeller)
	userUp.POST("/upgrade-admin", controller.UpgradeAdmin)
	userUp.DELETE("/delete/:id", controller.DeleteUser)

	r.GET("/like/:product_id", controller.GetLikebyProduct)
	r.GET("/comment/:product_id", controller.GetCommentbyProduct)

	likeMiddleware := r.Group("/like")
	likeMiddleware.Use(middlewares.JwtAuthMiddleWare())
	likeMiddleware.POST("/:product_id", controller.PostLike)
	likeMiddleware.DELETE("/:product_id", controller.Deletelike)

	commentMiddleware := r.Group("/comment")
	commentMiddleware.Use(middlewares.JwtAuthMiddleWare())
	commentMiddleware.POST("/:product_id", controller.PostComment)
	commentMiddleware.DELETE("/:product_id", controller.DeleteComment)

	productMiddleware := r.Group("/products")
	productMiddleware.Use(middlewares.SellerMiddleWare())
	productMiddleware.POST("/", controller.CreateProduct)
	productMiddleware.PATCH("/:id", controller.UpdateProduct)
	productMiddleware.DELETE("/:id", controller.DeleteProduct)

	r.GET("/categories", controller.GetAllCategory)
	r.GET("/categories/:id", controller.GetCategoryById)
	r.GET("/categories/:id/products", controller.GetProductByCategoryId)

	catagoriesMiddleware := r.Group("/categories")
	catagoriesMiddleware.Use(middlewares.SellerMiddleWare())
	catagoriesMiddleware.POST("/", controller.CreatingCategory)
	catagoriesMiddleware.PATCH("/:id", controller.UpdateCategory)
	catagoriesMiddleware.DELETE("/:id", controller.DeleteCategory)

	profileMiddleware := r.Group("/profile")
	profileMiddleware.Use(middlewares.JwtAuthMiddleWare())
	profileMiddleware.POST("/", controller.CreateProfile)
	profileMiddleware.GET("/", controller.GetProfile)
	profileMiddleware.PATCH("/", controller.UpdateProfile)
	profileMiddleware.DELETE("/", controller.DeleteProfile)

	userpaymentmiddleware := r.Group("/user-payment")
	userpaymentmiddleware.Use(middlewares.JwtAuthMiddleWare())
	userpaymentmiddleware.POST("/", controller.CreateUserpayment)
	userpaymentmiddleware.GET("/", controller.GetUserpayment)
	userpaymentmiddleware.PATCH("/", controller.UpdateUserpayment)
	userpaymentmiddleware.DELETE("/", controller.DeleteUserpayment)

	ordermiddleware := r.Group("/order")
	ordermiddleware.Use(middlewares.JwtAuthMiddleWare())
	ordermiddleware.POST("/:productID", controller.CreateOrder)
	ordermiddleware.GET("/", controller.GetOrder)
	ordermiddleware.PATCH("/:productID", controller.UpdateOrdeer)
	ordermiddleware.DELETE("/:productID", controller.DeleteOrder)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return r
}
